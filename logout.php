<?php 
session_start();

//détruire la session courante
session_destroy();

/* Redirige vers la page d'accueil */
$serveur = $_SERVER['HTTP_HOST'];
$chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$page = 'index.php';
header("Location: http://$serveur$chemin/$page");
?>