<?php
require "admin/bdd/bddconfig.php";
//activer les sessions
session_start();

//Tester si les variables POST existent
$paramOK = false;
if (isset($_POST["login"])) {
    $login = strtolower(htmlspecialchars($_POST["login"]));
    if (isset($_POST["password"])) {
        $password = htmlspecialchars($_POST["password"]);
        $paramOK = true;
    }
}

//si login et password sont bien reçus
if ($paramOK == true) {
    // vérifier si le login passord est correct (base de données) : voir après
    // echo $login." - ".$password;
    try {
        $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;
    charset=utf8", $bddlogin, $bddpass);
        $PDOlistlogins = $objBdd->prepare("SELECT * FROM user WHERE login = :login ");
        $PDOlistlogins->bindParam(':login', $login, PDO::PARAM_STR);
        $PDOlistlogins->execute();
    } catch (Exception $prmE) {
        die('Erreur ; ' . $prmE->getMessage());
    }

    //S'il y a un résultat à la requête 
    $row_userweb = $PDOlistlogins->fetch();

    // récuperation du pass hashé
    $hash = $row_userweb['password'];

    if (($row_userweb != false) && (password_verify($password, $hash))) {
        // il existe un login identique dans la base
        // vérif du password : voir après
        // remplace par :
        $_SESSION['logged_in']['idUser']=$row_userweb['idUser'];
        $_SESSION['logged_in']['login']=$row_userweb['login'];
        $_SESSION['logged_in']['nom']=$row_userweb['nom'];
        $_SESSION['logged_in']['fonction']=$row_userweb['fonction'];
        $_SESSION['logged_in']['idUser']=$row_userweb['idUser'];
        $serveur = $_SERVER['HTTP_HOST'];
        $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
        $page = 'index.php';
        header("Location: http://$serveur$chemin/$page");
    } else {
        //Mauvais login
        $titre = "Base de connaissance";
        ob_start(); ?>
        <p><a href="login.php">Se connecter</a></p>
        <p>Authentification incorrecte</p>
        <?php
        $contenu = ob_get_clean();
        if (isset($_SESSION['logged_in']['login']) == TRUE) {
            session_destroy();
        }
        require 'gabarit/template.php';
        die('Authentification incorrecte');
    }
} else {
    die('Vous devez fournir un login et un mot de passe');
}
//$row_userweb->closeCursor(); //libère les ressources de la bdd