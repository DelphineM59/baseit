<?php
// recuperation des variables passees en parametre pour afficher les bonnes informations
// connexion à la base
session_start();
require "admin/bdd/bddconfig.php";


try {
    $objBdd = new PDO(
        "mysql:host=$bddserver;
            dbname=$bddname;
            charset=utf8",
        $bddlogin,
        $bddpass
    );

    $objBdd->setAttribute(
        PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION
    );
    // affichage du titre de la page : 
    $titre = "Base de connaissance";
    ob_start();
    // traitement de la liste si l'acces est privé ou public
    if ((isset($_SESSION['logged_in']['login']) == TRUE) && ($_SESSION['logged_in']['fonction']=='tech')){
            $listearticles = $objBdd->query("SELECT *,theme.nom as nomtheme FROM article,theme,user where article.idTheme=theme.idTheme and article.idUser=user.idUser order by datePub desc limit 5");
        }else {
            $listearticles = $objBdd->query("SELECT *,theme.nom as nomtheme FROM article,theme,user where article.idTheme=theme.idTheme and article.idUser=user.idUser and article.access='public' order by datePub desc limit 5");
        }
    
?>
    <article>
        <h2>Derniers articles publiés</h2>
        <?php while ($unarticle = $listearticles->fetch()) {  
            ?>
            <p><a href="article.php?idtheme=<?= $unarticle['idTheme']; ?>&idarticle=<?= $unarticle['idArticle']; ?>">&#9655; <?= $unarticle['nomtheme']; ?> : <?= $unarticle['titre']; ?></a> édité le <?= $unarticle['datePub']; ?> par <?= $unarticle['nom']; ?></p>
        <?php } ?>
    </article>
    <?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php';
} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}

