<?php
// connexion à la base

require "admin/bdd/bddconfig.php";
session_start();
// recuperation des variables post
$titreok = isset($_POST["titre"]);
$texteok = isset($_POST["texte"]);
$idarticleok = isset($_POST["idarticle"]);
$accessok = isset($_POST["access"]);

// securisation des variables
if (($titreok) && ($texteok) && ($idarticleok) && ($accessok) ) {
    $titre = strval(htmlspecialchars($_POST["titre"]));
    $idarticle = intval(htmlspecialchars($_POST["idarticle"]));
    $texte = strval(htmlspecialchars($_POST["texte"]));
    $access = intval(htmlspecialchars($_POST["access"]));
           
    // insert dans la base
    // requete sql
    try {
        ob_start();
        $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;
        charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        if (isset($_POST['maj'])) { 
            // on fait une maj de la table article
            $RSLOGIN = $objBdd->prepare("update article set titre=:titre,texte=:texte,access=:access where idArticle=:idArticle");
            $RSLOGIN->bindParam(':titre', $titre, PDO::PARAM_STR);
            $RSLOGIN->bindParam(':texte', $texte, PDO::PARAM_STR);
            $RSLOGIN->bindParam(':access', $access, PDO::PARAM_INT);
            $RSLOGIN->bindParam(':idArticle', $idarticle, PDO::PARAM_INT);
            $RSLOGIN->execute();

            // recupérer la valeur de l'id du nouvel article crée
            // $lastId = $objBdd->lastInsertId();
        ?>
            <p></p>
            <article>
            <p>L'article sur <?= $titre ?> a bien été modifié.</p>
            <a href="index.php">Retour à la page d'accueil</a>
        </article>
        <?php  
        } elseif (isset($_POST['supr'])) { 
        // on supprime les champs de la table document
        $RSLOGIN = $objBdd->prepare("DELETE FROM document where idArticle=:idArticle");
        $RSLOGIN->bindParam(':idArticle', $idarticle, PDO::PARAM_INT);
        $RSLOGIN->execute();

        // on supprime les champs de la table article
        $RSLOGIN = $objBdd->prepare("DELETE FROM article where idArticle=:idArticle");
        $RSLOGIN->bindParam(':idArticle', $idarticle, PDO::PARAM_INT);
        $RSLOGIN->execute();
        ?>
            <p></p>
            <article>
            <p>L'article sur <?= $titre ?> a bien été supprimé.</p>
            <a href="index.php">Retour à la page d'accueil</a>
        </article>
        <?php  
    } 
        ?>

        <?php $contenu = ob_get_clean(); ?>
<?php 
    } catch (Exception $prmE) {
        die('Erreur ; ' . $prmE->getMessage());
    }

    // Redirection
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $page = 'index.php';
    //header("Location: http://$serveur$chemin/$page");

} else {
    die('Les paramètres ne sont pas valides');
}    
require 'gabarit/template.php';



