<?php
require "admin/bdd/bddconfig.php";
$titre = "Base de connaissance : Ajout d'un article";
// activer l'utilisation des variables de session
session_start();
ob_start(); 
// recuperation variables du lien pour idtheme/
$idthemeok = isset($_GET["idtheme"]);
// securisation des variables
if ($idthemeok) {
    $idtheme = intval(htmlspecialchars($_GET["idtheme"]));
    try {
        $objBdd = new PDO(
            "mysql:host=$bddserver;
            dbname=$bddname;
            charset=utf8",
            $bddlogin,
            $bddpass
        );

        $objBdd->setAttribute(
            PDO::ATTR_ERRMODE,
            PDO::ERRMODE_EXCEPTION
        );
        // affichage du theme de l'article : 
        $lesthemes = $objBdd->query("SELECT nom FROM theme where theme.idTheme=$idtheme");
        while ($letheme = $lesthemes->fetch()) {
            $nomTheme = $letheme['nom'];
        }
    } catch (Exception $prmE) {
        die('Erreur : ' . $prmE->getMessage());
    }
}
?>
<h2>Ajout d'un article dans le thème <?= $nomTheme; ?></h2>    
<div class="contain-form2">
<form method="POST" action="ajout_article_action.php" id="form-contain2">
    <Label for="titre">Titre</Label>
    <input type="text" name="titre" placeholder="Saisissez le titre de l'article..." required>
    <textarea name="texte" cols="80" rows="40" placeholder="Le texte..."></textarea>
    <Label for="access">Type d'access : </Label>
    <select name="access" id="">
    <option value="1">public</option>
    <option value="2">private</option>
    </select>
    <input type="texte" name="nomDocument" placeholder="Saisissez le nom du document..." required>
    <select name="type" id="">
    <option value="1">img</option>
    <option value="2">fichier</option>
    <option value="3">lien</option>
    </select>
    <input type="hidden" name="idTheme" value="<?= $idtheme; ?>">
    <input id="btn" type="submit" value="Ajouter l'article">
</form>
</div>


<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php' ?>