<?php
require "admin/bdd/bddconfig.php";
$titre = "Base de connaissance : Mise à jour d'un article";
// activer l'utilisation des variables de session
session_start();
ob_start(); 
// recuperation variables du lien pour idtheme/
$idarticleok = isset($_GET["idarticle"]);
// securisation des variables
if ($idarticleok) {
    $idarticle = intval(htmlspecialchars($_GET["idarticle"]));
    try {
        $objBdd = new PDO(
            "mysql:host=$bddserver;
            dbname=$bddname;
            charset=utf8",
            $bddlogin,
            $bddpass
        );

        $objBdd->setAttribute(
            PDO::ATTR_ERRMODE,
            PDO::ERRMODE_EXCEPTION
        );
        // affichage du theme de l'article : 
        $lesarticles = $objBdd->query("SELECT * FROM article where idArticle=$idarticle");
        $unarticle = $lesarticles->fetch();
            // affichage du formulaire avec les donnees de la base
            ?>
            <h2>Modification d'un article :</h2>    
            <div class="contain-form2">
                <form method="POST" action="maj_article_action.php" id="form-contain2" onsubmit="return confirm('Voulez vous vraiment supprimer l\'article ?');">
                    <Label for="titre">Titre</Label>
                    <input type="text" name="titre" id="input-titre" value="<?= $unarticle['titre']; ?>" required>
                    <Label for="access">Type d'access : </Label>
                    <select name="access" id="">
                        <option value="1">public</option>
                        <option value="2">private</option>
                    </select>
                    <textarea name="texte" cols="80" rows="40"><?= $unarticle['texte']; ?></textarea>
                    <input type="hidden" name="idarticle" value="<?= $idarticle; ?>">
                    <input class="btn" type="submit" name="maj" value="Mettre à jour">
                    <input class="btn" type="submit" name="supr" value="Supprimer">
                </form>
            </div>
        <?php 
    } catch (Exception $prmE) {
        die('Erreur : ' . $prmE->getMessage());
    }
}
?>


<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php' ?>