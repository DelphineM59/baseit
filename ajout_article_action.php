<?php
// connexion à la base

require "admin/bdd/bddconfig.php";
session_start();
// recuperation des variables post
$titreok = isset($_POST["titre"]);
$idThemeok = isset($_POST["idTheme"]);
$texteok = isset($_POST["texte"]);
$accessok = isset($_POST["access"]);
$nomDocumentok = isset($_POST["nomDocument"]);
$idsuerok = isset($_SESSION['logged_in']['iduser']);

//echo "Iduser : ".isset($_SESSION['logged_in']['iduser']);
// securisation des variables
if (($titreok) && ($idThemeok) && ($texteok) && ($accessok) && ($nomDocumentok) ) {
    $titre = strval(htmlspecialchars($_POST["titre"]));
    $nomDocument = strval(htmlspecialchars($_POST["nomDocument"]));
    $idTheme = intval(htmlspecialchars($_POST["idTheme"]));
    $texte = strval(htmlspecialchars($_POST["texte"]));
    $access = intval(htmlspecialchars($_POST["access"]));
    $type = intval(htmlspecialchars($_POST["type"]));
    $idUser = $_SESSION['logged_in']['idUser'];
    
    // insert dans la base
    // requete sql
    try {
        $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;
    charset=utf8", $bddlogin, $bddpass);
        $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // ajout d'un article
        $RSLOGIN = $objBdd->prepare("insert INTO article (titre,texte,access,idTheme,idUser) VALUES (:titre,:texte,:access,:idTheme,:idUser)");
        $RSLOGIN->bindParam(':titre', $titre, PDO::PARAM_STR);
        $RSLOGIN->bindParam(':texte', $texte, PDO::PARAM_STR);
        $RSLOGIN->bindParam(':access', $access, PDO::PARAM_INT);
        $RSLOGIN->bindParam(':idTheme', $idTheme, PDO::PARAM_INT);
        $RSLOGIN->bindParam(':idUser', $idUser, PDO::PARAM_INT);
        $RSLOGIN->execute();

        // recupérer la valeur de l'id du nouvel article crée
        $lastId = $objBdd->lastInsertId();

        // ajout d'un document
        $RSLOGIN = $objBdd->prepare("insert INTO document (nom,url,type,idArticle) VALUES (:nom,:url,:type,:idArticle)");
        $RSLOGIN->bindParam(':nom', $titre, PDO::PARAM_STR);
        $RSLOGIN->bindParam(':url', $nomDocument, PDO::PARAM_STR);
        $RSLOGIN->bindParam(':type', $type, PDO::PARAM_INT);
        $RSLOGIN->bindParam(':idArticle', $lastId, PDO::PARAM_INT);
        $RSLOGIN->execute();
        ob_start();
        ?>
        <article>
            L'article sur <?= $titre ?> a bien été ajouté
            <a href="index.php">Retour à la page d'accueil</a>
        </article>
        <?php $contenu = ob_get_clean(); ?>
<?php 
    } catch (Exception $prmE) {
        die('Erreur ; ' . $prmE->getMessage());
    }

    // Redirection
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $page = 'bassins.php';
    //header("Location: http://$serveur$chemin/$page");

} else {
    die('Les paramètres ne sont pas valides');
}    
require 'gabarit/template.php';


// rediriger automatiquement vers la page bassins.php
