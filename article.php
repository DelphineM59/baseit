<?php
session_start();

// recuperation des variables passees en parametre pour afficher les bonnes informations
// connexion à la base
require "admin/bdd/bddconfig.php";

// recuperation des 3 variables post
$idthemeok = isset($_GET["idtheme"]);
$idarticleok = isset($_GET["idarticle"]);

// securisation des variables
if (($idthemeok) && ($idarticleok)) {
    $idtheme = intval(htmlspecialchars($_GET["idtheme"]));
    $idarticle = intval(htmlspecialchars($_GET["idarticle"]));
    try {
        $objBdd = new PDO(
            "mysql:host=$bddserver;
            dbname=$bddname;
            charset=utf8",
            $bddlogin,
            $bddpass
        );

        $objBdd->setAttribute(
            PDO::ATTR_ERRMODE,
            PDO::ERRMODE_EXCEPTION
        );
        // affichage du titre de la page : 
        $lestitres = $objBdd->query("SELECT titre,nom FROM article,theme where idArticle=$idarticle and theme.idTheme=$idtheme");
        while ($letitre = $lestitres->fetch()) {
            $titre = $letitre['nom'];
            $nomArticle = $letitre['titre'];
        }
        ob_start();
        $contenuArticles = $objBdd->query("SELECT * FROM article,theme,user where idArticle=$idarticle and theme.idTheme=$idtheme and article.idUser=user.idUser");
?>
            <h2><?= $titre; ?> / <?= $nomArticle; ?></h2>
            <?php 
                while ($contenuArticle = $contenuArticles->fetch()) {  ?>
                <h3><?= $contenuArticle['titre']; ?></h3>
                <?php // recuperation de l'auteur du site
                      // comparaison avec le login connecté
                    if ((isset($_SESSION['logged_in']['idUser']) == TRUE) && ($_SESSION['logged_in']['idUser']==$contenuArticle['idUser'])){
                ?>
                    <div class="liensdroite">
                    <a href="maj_article.php?idarticle=<?= $idarticle; ?>">Mettre à jour / Supprimer</a>
                    </div>
                <?php } ?>
                <h4>Publié par <?= $contenuArticle['nom']; ?> le <?= date("d/m/Y", strtotime($contenuArticle['datePub'])); ?></h4>
                <article>
                <?= nl2br($contenuArticle['texte']); ?>
            <?php } ?>
                </article>
                <?php
                // declaration des tableaux pour le contenu de document
                $urlDocument = array();
                $nomDocument = array();
                $typeDocument = array();

                // on parcours la table, on conserve le contenu dans le tableau $contenuDocument  
                $documentArticle = $objBdd->query("SELECT * FROM article,document where article.idArticle=$idarticle and article.idArticle=document.idArticle");
                while ($donnees = $documentArticle->fetch()) {
                    $urlDocument[]=$donnees['url'];
                    $nomDocument[]=$donnees['nom'];
                    $typeDocument[]=$donnees['type'];
                }
                ?>
                    <?php for ($i=0; $i<count($typeDocument);$i++){
                        if ($typeDocument[$i]=='lien'){?>
                        <p><a href="<?= $urlDocument[$i]; ?>" target="_blank">Lien vers <?= $nomDocument[$i]; ?></a></p>
                    <?php }} ?> 
                    <div id="liens_images">
                    <?php for ($i=0; $i<count($typeDocument);$i++){
                        if ($typeDocument[$i]=='img'){?>
                        <img class="div-image-article" src="images/<?= $urlDocument[$i]; ?>" alt="<?= $nomDocument[$i]; ?>">  
                    <?php }} ?>
                    </div>
                    <?php for ($i=0; $i<count($typeDocument);$i++){
                        if ($typeDocument[$i]=='fichier'){?>
                        <a href="articles/<?= $urlDocument[$i]; ?>" download="<?= $nomDocument[$i]; ?>">Télécharger l'article</a>
                    <?php }} ?> 


        <?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php';
    } catch (Exception $prmE) {
        die('Erreur : ' . $prmE->getMessage());
    }
} else {
    // redirection page d'accueil
    // rediriger uniquement vers la page bassin.php
    // header ("Location:http://localhost/truites/bassins.php");
    // remplace par :
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $page = 'index.php';
    header("Location: http://$serveur$chemin/$page");
    die('Les paramètres ne sont pas valides');
}
