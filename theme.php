<?php
session_start();

// recuperation des variables passees en parametre pour afficher les bonnes informations
// connexion à la base
require "admin/bdd/bddconfig.php";

// recuperation des 3 variables post
$idthemeok = isset($_GET["idtheme"]);

// securisation des variables
if ($idthemeok) {
    $idtheme = intval(htmlspecialchars($_GET["idtheme"]));

    try {
        $objBdd = new PDO(
            "mysql:host=$bddserver;
            dbname=$bddname;
            charset=utf8",
            $bddlogin,
            $bddpass
        );

        $objBdd->setAttribute(
            PDO::ATTR_ERRMODE,
            PDO::ERRMODE_EXCEPTION
        );
        // affichage du titre de la page : 
        
        $lesthemes = $objBdd->query("SELECT nom FROM theme where idtheme=$idtheme");
        while ($letheme = $lesthemes->fetch()) { 
            $titre = $letheme['nom'];
        }
        ob_start();
        $listearticles = $objBdd->query("SELECT * FROM article,user where idtheme=$idtheme and user.idUser=article.idUser");
        ?>
        <article>
        <h2><?= $titre; ?></h2>
        <?php while ($unarticle = $listearticles->fetch()) {  ?>
        <p><a href="article.php?idtheme=<?= $idtheme; ?>&idarticle=<?= $unarticle['idArticle']; ?>">&#9655; <?= $titre; ?> : <?= $unarticle['titre']; ?></a> /<?= $unarticle['nom']; ?> / <?= $unarticle['datePub']; ?></p>
        <?php } ?>
        </article>
        <?php
        // test si utilisateur loggé et technicien
        if ((isset($_SESSION['logged_in']['login']) == TRUE) && ($_SESSION['logged_in']['fonction']=='tech')){
        ?>
            <article>
            <a href="ajout_article.php?idtheme=<?= $idtheme; ?>">Ajout d'un article</a>
            </article>
        <?php } ?>
        <?php $contenu = ob_get_clean(); ?>
        <?php require 'gabarit/template.php';
    } catch (Exception $prmE) {
        die('Erreur : ' . $prmE->getMessage());
    }
} else {
    // redirection page d'accueil
    // rediriger uniquement vers la page bassin.php
    // header ("Location:http://localhost/truites/bassins.php");
    // remplace par :
    $serveur = $_SERVER['HTTP_HOST'];
    $chemin = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $page = 'index.php';
    header("Location: http://$serveur$chemin/$page");
    die('Les paramètres ne sont pas valides');
}
