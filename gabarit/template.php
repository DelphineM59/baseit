<?php
// activer l'utilisation des variables de session
//session_start();

require "admin/bdd/bddconfig.php";
$objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);

try {
    $objBdd = new PDO(
        "mysql:host=$bddserver;
        dbname=$bddname;
        charset=utf8",
        $bddlogin,
        $bddpass
    );

    $objBdd->setAttribute(
        PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION
    );
    $listethemes = $objBdd->query("SELECT * FROM theme");
} catch (Exception $prmE) {
    die('Erreur : ' . $prmE->getMessage());
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title><?php $titre; ?></title>
    <link rel="stylesheet" href="css/styles.css" />
</head>
<p class="login">
    <?php // verifie si le membre est authentifie
    if (isset($_SESSION['logged_in']['login']) == TRUE) {
        //l'internaute est authentifié
        echo $_SESSION['logged_in']['nom'] . ' ' . $_SESSION['logged_in']['login']."</br>";
        
        //affichage "Se déconnecter"(logout.php), "Prif", "Paramètres", etc ...
    ?>
        <a href="logout.php">Se déconnecter</a>
</p>
<?php

    } else {
        //Personne n'est authentifié 
        // affichage d'un lien pour se connecter
?>
    <a href="login.php">Connexion</a>
<?php
    } ?>
</p>

<body>
    <div id="conteneur">
        <header>
            <h1>Base de connaissance IT</h1>

        </header>

        <nav>
            <h1>Thèmes</h1>
            <ul>
                <li><a href="index.php">Accueil</a></li>
                <?php
                while ($untheme = $listethemes->fetch()) {
                ?>
                    <li><a href="theme.php?idtheme=<?= $untheme['idTheme']; ?>"><?= $untheme['nom']; ?></a></li>
                <?php
                } //fin du while
                ?>
            </ul>
        </nav>
        <section>
            <div id="main-section">
                <?php echo $contenu; ?>
            </div>
        </section>
    </div>
</body>

</html>