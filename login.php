<?php
session_start();
$titre = "Base de connaissance : Login";
// activer l'utilisation des variables de session
//session_start();
ob_start(); ?>
<h2>Formulaire de connexion</h2>
<div class="contain-form">
<form method="POST" action="login_action.php" id="form-contain">
    <input type="text" name="login" placeholder="Saisissez votre login..." required>
    <input type="password" name="password" placeholder="Mot de passe" required>
    <input class="btn" type="submit" value="Se connecter">
</form>
</div>
<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/template.php' ?>